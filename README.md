# Sample App

This is a sample java application that performs basic mathematical operations.

## gitlab CI/CD setup
- Create a `.gitlab-ci.yml` with three stages
    - build
    - test
    - deploy

- Push code to check the CI/CD pipeline

- Create a `.JenkinsFile` with four stages
    - checkout
    - build
    - test
    - deploy

## Jenkins pipeline setup
- Open jenkins and create a new pipeline project
- Under pipeline select `Pipeline script from SCM` for definition.
- Enter your gitlab repo link and select the branch where your code is located.
- Enter your jenkins file name as mentioned in your gitlab and save the configuration.
- Now click on build to run the jenkins file. 

## Output of gitlab CI/CD
![gilab CI/CD output](bin/image.png)

## output of jenkins pipeline
![alt text](bin/image1.png)